#!/bin/bash
sudo add-apt-repository -y ppa:openjdk-r/ppa
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get -y install openjdk-8-jdk
sudo apt-get -y install jenkins
